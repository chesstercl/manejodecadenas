﻿Public Class Form1
    'Programe el evento Load del formulario para que al cargar, aparezca la frase
    ' “Hola, que tal estas” dentro de la caja de texto cadena.
    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        TextBoxString.Text = "Hola, que tal estas"
    End Sub

    'Programe el botón revisar para que entregue los resultados solicitados en los
    'labels a partir de la cadena de texto especificada en Cadena.
    Private Sub ButtonRevisar_Click(sender As Object, e As EventArgs) Handles ButtonRevisar.Click
        Dim frase, que, a, tio As String

        frase = TextBoxString.Text
        que = "que"
        a = "a"
        tio = "tío"


        'La primera letra es:
        TextBoxFirstLetter.Text = StrDup(1, frase)

        'Las tres primeras letras son:
        TextBoxThreeLetter.Text = frase.Substring(0, 3)

        'La longitud del texto es:
        TextBoxLongitud.Text = Len(frase)

        'La posicion de "que" es:
        TextBoxPosicionQue.Text = InStr(frase, que)

        'La ultima 'a' esta en la posicion:
        TextBoxPosicionA.Text = InStrRev(frase, a)

        'En mayusculas
        TextBoxMayusculas.Text = UCase(frase)

        'En minusculas
        TextBoxMinusculas.Text = LCase(frase)

        'A la inversa
        TextBoxInversa.Text = StrReverse(frase)

        'Insertar al principio de la cadena: tío
        TextBoxInsertTio.Text = frase.Insert(0, tio)

        'borrar 6 primeras letras
        TextBoxDelete6.Text = frase.Remove(0, 6)

        'cambiar "estas" por ESTAMOS
        TextBoxChangeEstas.Text = frase.Replace("estas", "ESTAMOS")

        'Retomar los 4 primeros caracteres de la izquierda
        TextBoxReturnLeft.Text = frase.Substring(0, 4)

        'Retomar 4 caracteres a partir de la posicion 3
        TextBoxReturn3.Text = frase.Substring(3, 4)
    End Sub

    'Se programa el botón buscarSubC para ingresar un texto y se indica si se encuentra o no
    'en la cadena original
    'Ejemplo: subcadena: que encontrada :  true
    'Ejemplo: subcadena: dragon encontrada :  false
    Private Sub ButtonSearchSubString_Click(sender As Object, e As EventArgs) Handles ButtonSearchSubString.Click

        Dim frase, subString As String
        frase = TextBoxString.Text
        subString = TextBoxInsertSubString.Text

        If frase.Contains(subString) = True Then
            TextBoxFalseTrue.Text = "True"
        Else
            TextBoxFalseTrue.Text = "False"
        End If
    End Sub
End Class
