﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelTitle = New System.Windows.Forms.Label()
        Me.LabelString = New System.Windows.Forms.Label()
        Me.TextBoxString = New System.Windows.Forms.TextBox()
        Me.ButtonRevisar = New System.Windows.Forms.Button()
        Me.LabelFirstLetter = New System.Windows.Forms.Label()
        Me.TextBoxFirstLetter = New System.Windows.Forms.TextBox()
        Me.LabelThreeLetters = New System.Windows.Forms.Label()
        Me.TextBoxThreeLetter = New System.Windows.Forms.TextBox()
        Me.LabelLongitudText = New System.Windows.Forms.Label()
        Me.TextBoxLongitud = New System.Windows.Forms.TextBox()
        Me.LabelPosicionQue = New System.Windows.Forms.Label()
        Me.TextBoxPosicionQue = New System.Windows.Forms.TextBox()
        Me.LabelPosicionA = New System.Windows.Forms.Label()
        Me.TextBoxPosicionA = New System.Windows.Forms.TextBox()
        Me.LabelSubTitle = New System.Windows.Forms.Label()
        Me.LabelMayusculas = New System.Windows.Forms.Label()
        Me.TextBoxMayusculas = New System.Windows.Forms.TextBox()
        Me.LabelMinusculas = New System.Windows.Forms.Label()
        Me.TextBoxMinusculas = New System.Windows.Forms.TextBox()
        Me.LabelInversa = New System.Windows.Forms.Label()
        Me.TextBoxInversa = New System.Windows.Forms.TextBox()
        Me.LabelInsertTio = New System.Windows.Forms.Label()
        Me.TextBoxInsertTio = New System.Windows.Forms.TextBox()
        Me.LabelDelete6 = New System.Windows.Forms.Label()
        Me.TextBoxDelete6 = New System.Windows.Forms.TextBox()
        Me.LabelChangeEstas = New System.Windows.Forms.Label()
        Me.TextBoxChangeEstas = New System.Windows.Forms.TextBox()
        Me.LabelReturnleft = New System.Windows.Forms.Label()
        Me.TextBoxReturnLeft = New System.Windows.Forms.TextBox()
        Me.LabelReturn3 = New System.Windows.Forms.Label()
        Me.TextBoxReturn3 = New System.Windows.Forms.TextBox()
        Me.LabelInsertSubString = New System.Windows.Forms.Label()
        Me.TextBoxInsertSubString = New System.Windows.Forms.TextBox()
        Me.ButtonSearchSubString = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TextBoxFalseTrue = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'LabelTitle
        '
        Me.LabelTitle.AutoSize = True
        Me.LabelTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelTitle.Location = New System.Drawing.Point(58, 9)
        Me.LabelTitle.Name = "LabelTitle"
        Me.LabelTitle.Size = New System.Drawing.Size(181, 13)
        Me.LabelTitle.TabIndex = 0
        Me.LabelTitle.Text = "Formulario Manejo de Cadenas"
        '
        'LabelString
        '
        Me.LabelString.AutoSize = True
        Me.LabelString.Location = New System.Drawing.Point(12, 31)
        Me.LabelString.Name = "LabelString"
        Me.LabelString.Size = New System.Drawing.Size(50, 13)
        Me.LabelString.TabIndex = 1
        Me.LabelString.Text = "Cadena: "
        '
        'TextBoxString
        '
        Me.TextBoxString.Location = New System.Drawing.Point(69, 31)
        Me.TextBoxString.Name = "TextBoxString"
        Me.TextBoxString.Size = New System.Drawing.Size(113, 20)
        Me.TextBoxString.TabIndex = 2
        '
        'ButtonRevisar
        '
        Me.ButtonRevisar.Location = New System.Drawing.Point(206, 31)
        Me.ButtonRevisar.Name = "ButtonRevisar"
        Me.ButtonRevisar.Size = New System.Drawing.Size(75, 23)
        Me.ButtonRevisar.TabIndex = 3
        Me.ButtonRevisar.Text = "Revisar"
        Me.ButtonRevisar.UseVisualStyleBackColor = True
        '
        'LabelFirstLetter
        '
        Me.LabelFirstLetter.AutoSize = True
        Me.LabelFirstLetter.Location = New System.Drawing.Point(12, 66)
        Me.LabelFirstLetter.Name = "LabelFirstLetter"
        Me.LabelFirstLetter.Size = New System.Drawing.Size(96, 13)
        Me.LabelFirstLetter.TabIndex = 4
        Me.LabelFirstLetter.Text = "La primera letra es:"
        '
        'TextBoxFirstLetter
        '
        Me.TextBoxFirstLetter.Location = New System.Drawing.Point(181, 66)
        Me.TextBoxFirstLetter.Name = "TextBoxFirstLetter"
        Me.TextBoxFirstLetter.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxFirstLetter.TabIndex = 5
        '
        'LabelThreeLetters
        '
        Me.LabelThreeLetters.AutoSize = True
        Me.LabelThreeLetters.Location = New System.Drawing.Point(12, 103)
        Me.LabelThreeLetters.Name = "LabelThreeLetters"
        Me.LabelThreeLetters.Size = New System.Drawing.Size(137, 13)
        Me.LabelThreeLetters.TabIndex = 6
        Me.LabelThreeLetters.Text = "Las tres primeras letras son:"
        '
        'TextBoxThreeLetter
        '
        Me.TextBoxThreeLetter.Location = New System.Drawing.Point(181, 103)
        Me.TextBoxThreeLetter.Name = "TextBoxThreeLetter"
        Me.TextBoxThreeLetter.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxThreeLetter.TabIndex = 7
        '
        'LabelLongitudText
        '
        Me.LabelLongitudText.AutoSize = True
        Me.LabelLongitudText.Location = New System.Drawing.Point(12, 136)
        Me.LabelLongitudText.Name = "LabelLongitudText"
        Me.LabelLongitudText.Size = New System.Drawing.Size(119, 13)
        Me.LabelLongitudText.TabIndex = 8
        Me.LabelLongitudText.Text = "La longitud del texto es:"
        '
        'TextBoxLongitud
        '
        Me.TextBoxLongitud.Location = New System.Drawing.Point(181, 136)
        Me.TextBoxLongitud.Name = "TextBoxLongitud"
        Me.TextBoxLongitud.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxLongitud.TabIndex = 9
        '
        'LabelPosicionQue
        '
        Me.LabelPosicionQue.AutoSize = True
        Me.LabelPosicionQue.Location = New System.Drawing.Point(12, 172)
        Me.LabelPosicionQue.Name = "LabelPosicionQue"
        Me.LabelPosicionQue.Size = New System.Drawing.Size(124, 13)
        Me.LabelPosicionQue.TabIndex = 10
        Me.LabelPosicionQue.Text = "La posicion de ""que"" es:"
        '
        'TextBoxPosicionQue
        '
        Me.TextBoxPosicionQue.Location = New System.Drawing.Point(181, 169)
        Me.TextBoxPosicionQue.Name = "TextBoxPosicionQue"
        Me.TextBoxPosicionQue.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPosicionQue.TabIndex = 11
        '
        'LabelPosicionA
        '
        Me.LabelPosicionA.AutoSize = True
        Me.LabelPosicionA.Location = New System.Drawing.Point(12, 200)
        Me.LabelPosicionA.Name = "LabelPosicionA"
        Me.LabelPosicionA.Size = New System.Drawing.Size(156, 13)
        Me.LabelPosicionA.TabIndex = 12
        Me.LabelPosicionA.Text = "La ultima 'a' esta en la posicion:"
        '
        'TextBoxPosicionA
        '
        Me.TextBoxPosicionA.Location = New System.Drawing.Point(181, 200)
        Me.TextBoxPosicionA.Name = "TextBoxPosicionA"
        Me.TextBoxPosicionA.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxPosicionA.TabIndex = 13
        '
        'LabelSubTitle
        '
        Me.LabelSubTitle.AutoSize = True
        Me.LabelSubTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LabelSubTitle.Location = New System.Drawing.Point(12, 237)
        Me.LabelSubTitle.Name = "LabelSubTitle"
        Me.LabelSubTitle.Size = New System.Drawing.Size(395, 13)
        Me.LabelSubTitle.TabIndex = 14
        Me.LabelSubTitle.Text = "Despues de las siguientes operaciones, la cadena resultante queda:"
        '
        'LabelMayusculas
        '
        Me.LabelMayusculas.AutoSize = True
        Me.LabelMayusculas.Location = New System.Drawing.Point(12, 271)
        Me.LabelMayusculas.Name = "LabelMayusculas"
        Me.LabelMayusculas.Size = New System.Drawing.Size(81, 13)
        Me.LabelMayusculas.TabIndex = 15
        Me.LabelMayusculas.Text = "En mayúsculas:"
        '
        'TextBoxMayusculas
        '
        Me.TextBoxMayusculas.Location = New System.Drawing.Point(100, 271)
        Me.TextBoxMayusculas.Name = "TextBoxMayusculas"
        Me.TextBoxMayusculas.Size = New System.Drawing.Size(181, 20)
        Me.TextBoxMayusculas.TabIndex = 16
        '
        'LabelMinusculas
        '
        Me.LabelMinusculas.AutoSize = True
        Me.LabelMinusculas.Location = New System.Drawing.Point(12, 310)
        Me.LabelMinusculas.Name = "LabelMinusculas"
        Me.LabelMinusculas.Size = New System.Drawing.Size(78, 13)
        Me.LabelMinusculas.TabIndex = 17
        Me.LabelMinusculas.Text = "En minúsculas:"
        '
        'TextBoxMinusculas
        '
        Me.TextBoxMinusculas.Location = New System.Drawing.Point(100, 310)
        Me.TextBoxMinusculas.Name = "TextBoxMinusculas"
        Me.TextBoxMinusculas.Size = New System.Drawing.Size(181, 20)
        Me.TextBoxMinusculas.TabIndex = 18
        '
        'LabelInversa
        '
        Me.LabelInversa.AutoSize = True
        Me.LabelInversa.Location = New System.Drawing.Point(15, 347)
        Me.LabelInversa.Name = "LabelInversa"
        Me.LabelInversa.Size = New System.Drawing.Size(65, 13)
        Me.LabelInversa.TabIndex = 19
        Me.LabelInversa.Text = "A la inversa:"
        '
        'TextBoxInversa
        '
        Me.TextBoxInversa.Location = New System.Drawing.Point(100, 344)
        Me.TextBoxInversa.Name = "TextBoxInversa"
        Me.TextBoxInversa.Size = New System.Drawing.Size(181, 20)
        Me.TextBoxInversa.TabIndex = 20
        '
        'LabelInsertTio
        '
        Me.LabelInsertTio.AutoSize = True
        Me.LabelInsertTio.Location = New System.Drawing.Point(15, 376)
        Me.LabelInsertTio.Name = "LabelInsertTio"
        Me.LabelInsertTio.Size = New System.Drawing.Size(179, 13)
        Me.LabelInsertTio.TabIndex = 21
        Me.LabelInsertTio.Text = "Insertar al principio de la cadena: tío"
        '
        'TextBoxInsertTio
        '
        Me.TextBoxInsertTio.Location = New System.Drawing.Point(18, 392)
        Me.TextBoxInsertTio.Name = "TextBoxInsertTio"
        Me.TextBoxInsertTio.Size = New System.Drawing.Size(263, 20)
        Me.TextBoxInsertTio.TabIndex = 22
        '
        'LabelDelete6
        '
        Me.LabelDelete6.AutoSize = True
        Me.LabelDelete6.Location = New System.Drawing.Point(15, 424)
        Me.LabelDelete6.Name = "LabelDelete6"
        Me.LabelDelete6.Size = New System.Drawing.Size(117, 13)
        Me.LabelDelete6.TabIndex = 23
        Me.LabelDelete6.Text = "Borrar 6 primeras letras:"
        '
        'TextBoxDelete6
        '
        Me.TextBoxDelete6.Location = New System.Drawing.Point(18, 440)
        Me.TextBoxDelete6.Name = "TextBoxDelete6"
        Me.TextBoxDelete6.Size = New System.Drawing.Size(263, 20)
        Me.TextBoxDelete6.TabIndex = 24
        '
        'LabelChangeEstas
        '
        Me.LabelChangeEstas.AutoSize = True
        Me.LabelChangeEstas.Location = New System.Drawing.Point(15, 478)
        Me.LabelChangeEstas.Name = "LabelChangeEstas"
        Me.LabelChangeEstas.Size = New System.Drawing.Size(156, 13)
        Me.LabelChangeEstas.TabIndex = 25
        Me.LabelChangeEstas.Text = "Cambiar ""estas"" por ESTAMOS"
        '
        'TextBoxChangeEstas
        '
        Me.TextBoxChangeEstas.Location = New System.Drawing.Point(18, 494)
        Me.TextBoxChangeEstas.Name = "TextBoxChangeEstas"
        Me.TextBoxChangeEstas.Size = New System.Drawing.Size(263, 20)
        Me.TextBoxChangeEstas.TabIndex = 26
        '
        'LabelReturnleft
        '
        Me.LabelReturnleft.AutoSize = True
        Me.LabelReturnleft.Location = New System.Drawing.Point(13, 521)
        Me.LabelReturnleft.Name = "LabelReturnleft"
        Me.LabelReturnleft.Size = New System.Drawing.Size(239, 13)
        Me.LabelReturnleft.TabIndex = 27
        Me.LabelReturnleft.Text = "Retornar los 4 primeros caracteres de la izquierda"
        '
        'TextBoxReturnLeft
        '
        Me.TextBoxReturnLeft.Location = New System.Drawing.Point(16, 538)
        Me.TextBoxReturnLeft.Name = "TextBoxReturnLeft"
        Me.TextBoxReturnLeft.Size = New System.Drawing.Size(265, 20)
        Me.TextBoxReturnLeft.TabIndex = 28
        '
        'LabelReturn3
        '
        Me.LabelReturn3.AutoSize = True
        Me.LabelReturn3.Location = New System.Drawing.Point(13, 565)
        Me.LabelReturn3.Name = "LabelReturn3"
        Me.LabelReturn3.Size = New System.Drawing.Size(222, 13)
        Me.LabelReturn3.TabIndex = 29
        Me.LabelReturn3.Text = "Retornar 4 caracteres a partir de la posicion 3"
        '
        'TextBoxReturn3
        '
        Me.TextBoxReturn3.Location = New System.Drawing.Point(18, 581)
        Me.TextBoxReturn3.Name = "TextBoxReturn3"
        Me.TextBoxReturn3.Size = New System.Drawing.Size(268, 20)
        Me.TextBoxReturn3.TabIndex = 30
        '
        'LabelInsertSubString
        '
        Me.LabelInsertSubString.AutoSize = True
        Me.LabelInsertSubString.Location = New System.Drawing.Point(15, 609)
        Me.LabelInsertSubString.Name = "LabelInsertSubString"
        Me.LabelInsertSubString.Size = New System.Drawing.Size(102, 13)
        Me.LabelInsertSubString.TabIndex = 31
        Me.LabelInsertSubString.Text = "Ingresar subCadena"
        '
        'TextBoxInsertSubString
        '
        Me.TextBoxInsertSubString.Location = New System.Drawing.Point(19, 623)
        Me.TextBoxInsertSubString.Name = "TextBoxInsertSubString"
        Me.TextBoxInsertSubString.Size = New System.Drawing.Size(167, 20)
        Me.TextBoxInsertSubString.TabIndex = 32
        '
        'ButtonSearchSubString
        '
        Me.ButtonSearchSubString.Location = New System.Drawing.Point(192, 623)
        Me.ButtonSearchSubString.Name = "ButtonSearchSubString"
        Me.ButtonSearchSubString.Size = New System.Drawing.Size(100, 23)
        Me.ButtonSearchSubString.TabIndex = 33
        Me.ButtonSearchSubString.Text = "buscar subString"
        Me.ButtonSearchSubString.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(15, 658)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(171, 13)
        Me.Label1.TabIndex = 34
        Me.Label1.Text = "subcadena encontrada? false/true"
        '
        'TextBoxFalseTrue
        '
        Me.TextBoxFalseTrue.Location = New System.Drawing.Point(192, 655)
        Me.TextBoxFalseTrue.Name = "TextBoxFalseTrue"
        Me.TextBoxFalseTrue.Size = New System.Drawing.Size(100, 20)
        Me.TextBoxFalseTrue.TabIndex = 35
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(418, 655)
        Me.Controls.Add(Me.TextBoxFalseTrue)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.ButtonSearchSubString)
        Me.Controls.Add(Me.TextBoxInsertSubString)
        Me.Controls.Add(Me.LabelInsertSubString)
        Me.Controls.Add(Me.TextBoxReturn3)
        Me.Controls.Add(Me.LabelReturn3)
        Me.Controls.Add(Me.TextBoxReturnLeft)
        Me.Controls.Add(Me.LabelReturnleft)
        Me.Controls.Add(Me.TextBoxChangeEstas)
        Me.Controls.Add(Me.LabelChangeEstas)
        Me.Controls.Add(Me.TextBoxDelete6)
        Me.Controls.Add(Me.LabelDelete6)
        Me.Controls.Add(Me.TextBoxInsertTio)
        Me.Controls.Add(Me.LabelInsertTio)
        Me.Controls.Add(Me.TextBoxInversa)
        Me.Controls.Add(Me.LabelInversa)
        Me.Controls.Add(Me.TextBoxMinusculas)
        Me.Controls.Add(Me.LabelMinusculas)
        Me.Controls.Add(Me.TextBoxMayusculas)
        Me.Controls.Add(Me.LabelMayusculas)
        Me.Controls.Add(Me.LabelSubTitle)
        Me.Controls.Add(Me.TextBoxPosicionA)
        Me.Controls.Add(Me.LabelPosicionA)
        Me.Controls.Add(Me.TextBoxPosicionQue)
        Me.Controls.Add(Me.LabelPosicionQue)
        Me.Controls.Add(Me.TextBoxLongitud)
        Me.Controls.Add(Me.LabelLongitudText)
        Me.Controls.Add(Me.TextBoxThreeLetter)
        Me.Controls.Add(Me.LabelThreeLetters)
        Me.Controls.Add(Me.TextBoxFirstLetter)
        Me.Controls.Add(Me.LabelFirstLetter)
        Me.Controls.Add(Me.ButtonRevisar)
        Me.Controls.Add(Me.TextBoxString)
        Me.Controls.Add(Me.LabelString)
        Me.Controls.Add(Me.LabelTitle)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents LabelTitle As System.Windows.Forms.Label
    Friend WithEvents LabelString As System.Windows.Forms.Label
    Friend WithEvents TextBoxString As System.Windows.Forms.TextBox
    Friend WithEvents ButtonRevisar As System.Windows.Forms.Button
    Friend WithEvents LabelFirstLetter As System.Windows.Forms.Label
    Friend WithEvents TextBoxFirstLetter As System.Windows.Forms.TextBox
    Friend WithEvents LabelThreeLetters As System.Windows.Forms.Label
    Friend WithEvents TextBoxThreeLetter As System.Windows.Forms.TextBox
    Friend WithEvents LabelLongitudText As System.Windows.Forms.Label
    Friend WithEvents TextBoxLongitud As System.Windows.Forms.TextBox
    Friend WithEvents LabelPosicionQue As System.Windows.Forms.Label
    Friend WithEvents TextBoxPosicionQue As System.Windows.Forms.TextBox
    Friend WithEvents LabelPosicionA As System.Windows.Forms.Label
    Friend WithEvents TextBoxPosicionA As System.Windows.Forms.TextBox
    Friend WithEvents LabelSubTitle As System.Windows.Forms.Label
    Friend WithEvents LabelMayusculas As System.Windows.Forms.Label
    Friend WithEvents TextBoxMayusculas As System.Windows.Forms.TextBox
    Friend WithEvents LabelMinusculas As System.Windows.Forms.Label
    Friend WithEvents TextBoxMinusculas As System.Windows.Forms.TextBox
    Friend WithEvents LabelInversa As System.Windows.Forms.Label
    Friend WithEvents TextBoxInversa As System.Windows.Forms.TextBox
    Friend WithEvents LabelInsertTio As System.Windows.Forms.Label
    Friend WithEvents TextBoxInsertTio As System.Windows.Forms.TextBox
    Friend WithEvents LabelDelete6 As System.Windows.Forms.Label
    Friend WithEvents TextBoxDelete6 As System.Windows.Forms.TextBox
    Friend WithEvents LabelChangeEstas As System.Windows.Forms.Label
    Friend WithEvents TextBoxChangeEstas As System.Windows.Forms.TextBox
    Friend WithEvents LabelReturnleft As System.Windows.Forms.Label
    Friend WithEvents TextBoxReturnLeft As System.Windows.Forms.TextBox
    Friend WithEvents LabelReturn3 As System.Windows.Forms.Label
    Friend WithEvents TextBoxReturn3 As System.Windows.Forms.TextBox
    Friend WithEvents LabelInsertSubString As System.Windows.Forms.Label
    Friend WithEvents TextBoxInsertSubString As System.Windows.Forms.TextBox
    Friend WithEvents ButtonSearchSubString As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TextBoxFalseTrue As System.Windows.Forms.TextBox

End Class
